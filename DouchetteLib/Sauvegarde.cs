﻿using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Text;

namespace DouchetteLib
{
    public static class Sauvegarde
    {
        /// <summary>
        /// Call Update_data if data if already in base and Insert_data if it isn't
        /// </summary>
        /// <param name="list_data">list of the bar code split parts</param>
        /// <param name="is_in_base">boolean for the presence in base</param>
        public static void Save_data(List<String> list_data, bool is_in_base)
        {
            if (is_in_base)
                Update_data(list_data);
            else
                Insert_data(list_data);
        }

        /// <summary>
        /// Insertion of the data in the data base
        /// </summary>
        /// <param name="list_data">list of the bar code split parts to insert in base</param>
        public static void Insert_data(List<String> list_data)
        {
            SqliteConnection con = new SqliteConnection("Data source=../../../../database.sqlite3");
            con.Open();
            String query = "INSERT INTO Product VALUES(" + list_data[0] + ", " + list_data[1] + ", " + list_data[2] + ")";
            SqliteCommand cursor = new SqliteCommand(query, con);
            cursor.ExecuteNonQuery();
            con.Close();
        }

        /// <summary>
        /// Update the data in the data base
        /// </summary>
        /// <param name="list_data">list of the bar code split parts to update in base</param>
        public static void Update_data(List<String> list_data)
        {
            SqliteConnection con = new SqliteConnection("Data source=../../../../database.sqlite3");
            con.Open();
            String query = "UPDATE Product " +
                           "SET type='" + list_data[0] + "', prod_year='" + list_data[1] + "', prod_ref='" + list_data[2] + "'" +
                           "WHERE prod_ref='" + list_data[2] + "'";
            SqliteCommand cursor = new SqliteCommand(query, con);
            cursor.ExecuteNonQuery();
            con.Close();
        }
    }
}
