﻿using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;


namespace DouchetteLib
{
    public static class Verification
    {
        /// <summary>
        /// Split the data and check the conformity of each part
        /// </summary>
        /// <param name="type_year_ref">bar code</param>
        /// <returns> a tab of boolean</returns>
        public static bool[] VerificationCode(String type_year_ref)
        {
            bool[] tab = new bool[] {false, false};

            List<String> list_data = Split_data(type_year_ref);
            tab[0] = Verification_data(list_data);
            if (tab[0] == true)
                tab[1] = Verification_base(list_data);

            return tab;
        }

        /// <summary>
        /// Split the data with '-' as delimiter
        /// </summary>
        /// <param name="type_year_ref">bar code</param>
        /// <returns> the list of data after the split</returns>
        public static List<String> Split_data(String type_year_ref)
        {
            String[] tab_data = type_year_ref.Split('-');
            List<String> list_data = new List<String>();
            foreach(String data in tab_data)
            {
                list_data.Add(data);
            }

            return list_data;
        }

        /// <summary>
        /// Call the differents function of verification for the list of data
        /// </summary>
        /// <param name="list_data">Bar Code</param>
        /// <returns>the boolean result of each verication call</returns>
        public static bool Verification_data(List<String> list_data)
        {
            bool data_lenght = Verif_data_lenght(list_data);
            bool data_nb_letters = Verif_data_nb_letters(list_data[0]);
            bool data_nb_numbers = Verif_data_nb_numbers(list_data);

            return data_lenght && data_nb_letters && data_nb_numbers;
        }

        /// <summary>
        /// Check if the data is arlready in base
        /// </summary>
        /// <param name="list_data">Bar Code</param>
        /// <returns>boolean result</returns>
        public static bool Verification_base(List<String> list_data)
        {
            bool is_in_base = true;
            SqliteConnection con = new SqliteConnection("Data source=../../../../database.sqlite3");
            con.Open();
            String query = "SELECT type FROM Product WHERE prod_ref=" + list_data[2];
            SqliteCommand cursor = new SqliteCommand(query, con);
            SqliteDataReader result = cursor.ExecuteReader();
            is_in_base = result.HasRows;
            con.Close();
            return is_in_base;
        }

        /// <summary>
        /// Check if list_data have the 3 parts of the bar code and 3 part only
        /// </summary>
        /// <param name="list_data"></param>
        /// <returns>boolean result</returns>
        public static bool Verif_data_lenght(List<String> list_data)
        {
            if (list_data.Count == 3)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Check if there is letters in the data
        /// </summary>
        /// <param name="data"></param>
        /// <returns>boolean result</returns>
        public static bool Verif_data_nb_letters(String data)
        {
            bool nb_letters = false;
            if (data.Length == 3)
            {
                nb_letters = true;
                foreach (char caractere in data)
                    if (caractere < 65 || caractere > 90)
                        nb_letters = false;
            }
            return nb_letters;
        }

        /// <summary>
        /// Check if there is number in the data
        /// </summary>
        /// <param name="list_data"></param>
        /// <returns>boolean result</returns>
        public static bool Verif_data_nb_numbers(List<String> list_data)
        {
            bool nb_numbers = false;
            if(list_data[1].Length == 4 && list_data[2].Length == 4)
            {
                nb_numbers = true;
                foreach (char number in list_data[1])
                    if (number < 48 && number > 57)
                        nb_numbers = false;

                foreach (char number in list_data[2])
                    if (number < 48 && number > 57)
                        nb_numbers = false;
            }
            return nb_numbers;
        }
    }
}
