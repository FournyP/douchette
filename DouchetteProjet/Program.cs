﻿using System;
using System.IO;
using Microsoft.Data.Sqlite;
using DouchetteLib;

namespace DouchetteProjet
{
    class Program
    {
        static void Main(string[] args)
        {
            //Création de la base de donnée
            /* 
            SqliteCommand cursor = new SqliteCommand("CREATE TABLE Product(type,prod_year,prod_ref)", con);
            cursor.ExecuteNonQuery();
            */
            String type_year_ref = "PER-2019-5869";

            bool[] tab = DouchetteLib.Verification.VerificationCode(type_year_ref);
            bool OK = tab[0];
            bool is_in_base = tab[1];

            if (OK)
                DouchetteLib.Sauvegarde.Save_data(DouchetteLib.Verification.Split_data(type_year_ref), is_in_base);
        }
    }
}
